import pandas as pd
import numpy as np
import os, sys, re, h5py
import matplotlib.pyplot as plt

from scipy.optimize import *
from scipy.integrate import *
from scipy.signal import *

import FuncLib as Func

hlowcut = 200
hhighcut = 500
llowcut = 200
lhighcut = 500
fs = 100e+3
order = 1
conversion_factor = 10./2**18
base_end = 17200

PATH = '/home/wootaekim/work/h5py_read/H5PROD_TEST/'
h5file = PATH+'test_0.h5'

pd_heat = [pd.DataFrame({'time' : [0], 'xhpeak' : [0], 'hpeak' : [0], 'hrt90_h' : [0], 'hrt50_h' : [0], 'hrt10_h' : [0], 'hrt90' : [0], 'hrt50' : [0], 'hrt10' : [0], 'hpedestal' : [0], 'hendline' : [0], 'hendlineRMS' : [0]}) for _ in range(4)]
pd_light = [pd.DataFrame({'time' : [0], 'xlpeak' : [0], 'lpeak' : [0], 'lrt90_h' : [0], 'lrt50_h' : [0], 'lrt10_h' : [0], 'lrt90' : [0], 'lrt50' : [0], 'lrt10' : [0], 'lpedestal' : [0], 'lendline' : [0], 'lendlineRMS' : [0]}) for _ in range(4)]
pd_heat_band = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0]}) for _ in range(4)]
pd_light_band = [pd.DataFrame({'time': [0], 'xlfpeak' : [0], 'lfpeak' : [0], 'lfbaseline': [0], 'lfbaselineRMS' : [0], 'xlfmin' : [0], 'lfmin' : [0], 'xlfpeak2nd' : [0], 'lfpeak2nd' : [0]}) for _ in range(4)]

with h5py.File(h5file, 'r') as f :
    dset = f['wave']
    print (dset.shape)
    for i in range(dset.shape[0]) :
        h = dset[i][2]
        l = dset[i][3]
        t = dset[i][1]
        chN = dset[i][0]
        
        #heat channel ana
        h_ana = Func.heat_ana(h, conversion_factor, fs, np.mean(h[14000:17000]), np.std(h[14000:17000]))
        #heat channel bandpass filter ana
        h_band_ana = Func.heat_filter_ana(hlowcut, hhighcut, order, h, conversion_factor, fs, base_end)
        #light channel ana
        l_ana = Func.light_ana(l, conversion_factor, fs, np.mean(l[14000:17000]), np.std(l[14000:17000]))
        #light channel bandpass filter ana
        l_band_ana = Func.light_filter_ana(llowcut, lhighcut, order, l, conversion_factor, fs, base_end)

        pd_h_ana = pd.DataFrame({'time' : t, 'xhpeak' : h_ana[0], 'hpeak' : h_ana[1], 'hrt90_h' : h_ana[2], 'hrt50_h' : h_ana[3], 'hrt10_h' : h_ana[3]\
                , 'hrt90' : h_ana[4], 'hrt50' : h_ana[5], 'hrt10' : h_ana[6]\
                , 'hpedestal' : h_ana[-1], 'hendline' : h_ana[-4], 'hendlineRMS' : h_ana[-3]})
        pd_l_ana = pd.DataFrame({'time' : t, 'xlpeak' : l_ana[0], 'lpeak' : l_ana[1], 'lrt90_h' : l_ana[2], 'lrt50_h' : l_ana[3], 'lrt10_h' : l_ana[3]\
                , 'lrt90' : l_ana[4], 'lrt50' : l_ana[5], 'lrt10' : l_ana[6]\
                , 'lpedestal' : l_ana[-1], 'lendline' : l_ana[-4], 'lendlineRMS' : l_ana[-3]})
        pd_h_band_ana = pd.DataFrame({'time' : t, 'xhfpeak' : h_band_ana[0], 'hfpeak' : h_band_ana[1], 'hfbaseline' : h_band_ana[2], 'hfbaselineRMS' : h_band_ana[3]\
                , 'xhfmin' : h_band_ana[4], 'hfmin' : h_band_ana[5], 'xhfpeak2nd' : h_band_ana[6], 'hfpeak2nd' : h_band_ana[7]})
        pd_l_band_ana = pd.DataFrame({'time' : t, 'xlfpeak' : l_band_ana[0], 'lfpeak' : l_band_ana[1], 'lfbaseline' : l_band_ana[2], 'lfbaselineRMS' : l_band_ana[3]\
                , 'xlfmin' : l_band_ana[4], 'lfmin' : l_band_ana[5], 'xlfpeak2nd' : l_band_ana[6], 'lfpeak2nd' : l_band_ana[7]})
        for ii in range(4) :
            if (chN == ii) :
                pd_heat[ii] = pd_heat[ii].append(pd_h_ana, ignore_index=True)
                pd_light[ii] = pd_light[ii].append(pd_l_ana, ignore_index=True)
                pd_heat_band[ii] = pd_heat_band[ii].append(pd_h_band_ana, ignore_index=True)
                pd_light_band[ii] = pd_light_band[ii].append(pd_l_band_ana, ignore_index=True)


for i in range(4) :
    pd_heat[i].to_hdf(PATH+"_ch{0}.h5".format(i), key='heat', mode='w')
    pd_light[i].to_hdf(PATH+"_ch{0}.h5".format(i), key='light', mode='w')
    pd_heat_band[i].to_hdf(PATH+"_ch{0}.h5", key='heat_band', mode='w')
    pd_light_band[i].to_hdf(PATH+"_ch{0}.h5", key='light_band', mode='w')
#df.to_hdf('test_pd_h5.h5', key='df', mode='w')


