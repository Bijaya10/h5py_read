import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import *
from ROOT import TFile, TTree, TChain
import pandas as pd
from scipy.signal import *
from array import array
from scipy.signal import find_peaks
import h5py

###ButterWorth bandpass filter###
def butter_bandpass_sos(lowcut, highcut, fs, order =5):
    nyq = 0.5*fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter_sos(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass_sos(lowcut, highcut, fs, order=order)
    y = sosfilt(sos, data)
    return y

###ploynominal fuction###
def poly2(x, a, b, c):
    return a*x**2 + b*x +c

###Example READ PROD FILEs using H5py
def READ5PY(h5file) :
    with h5py.File(h5file, 'r') as f :
        print (f.keys)
        dset = f['wave']

###HEAT Channel anaylsis###
def heat_ana(heat_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
   

    try :
        #pulse height and height point
        grad_heat_array = np.gradient(heat_array)
        n_heat_array= movingaverage(grad_heat_array, 40)

        argtrg = np.argmax(n_heat_array[base_end:base_end+1500]) + base_end
        argtrg = int(argtrg - 70) 

        pedestal = np.mean(heat_array[argtrg-500:argtrg])
        hmin = np.min(heat_array[argtrg:argtrg+1000])
        peaks, _ = find_peaks(heat_array[argtrg:argtrg+1500], height=pedestal+10)
        xhpeaks = peaks+argtrg
        hpeak = np.max(heat_array[xhpeaks]) - pedestal
        arghpeak = np.argmax(heat_array[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        
        #For Risetime
        rt90 = hpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(heat_array[argtrg:xhpeak] - rt90))+argtrg

        rt50 = hpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(heat_array[argtrg:xhpeak] - rt50))+argtrg

        rt10 = hpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(heat_array[argtrg:xhpeak] - rt10))+argtrg
        
        #print (rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10)
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, heat_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, heat_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x

        ###End baseline
        endline = np.mean(heat_array[-2000:])
        endlineRMS = np.std(heat_array[-2000:])
        
        result = [xhpeak, hpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, hmin, pedestal]
    
    except ValueError :
        result = [-1]*15
        
    return result

###LIGHT Channel analysis###
   
def light_ana(light_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :

    try :
        #pulse height and height point
        grad_light_array = np.gradient(light_array)
        n_light_array= movingaverage(grad_light_array, 40)

        argtrg = np.argmax(n_light_array[base_end:base_end+1500]) + base_end
        argtrg = int(argtrg - 70) 

        pedestal = np.mean(light_array[argtrg-500:argtrg])
        lmin = np.min(light_array[argtrg:argtrg+1000])
        peaks, _ = find_peaks(light_array[argtrg:argtrg+1500], height=pedestal+10)
        xlpeaks = peaks+argtrg
        lpeak = np.max(light_array[xlpeaks]) - pedestal
        arglpeak = np.argmax(light_array[xlpeaks])
        xlpeak = xlpeaks[arglpeak]
        #result = [xlpeak, lpeak]
        
        #For Risetime
        rt90 = lpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(light_array[base_end:xlpeak] - rt90))+base_end

        rt50 = lpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(light_array[base_end:xlpeak] - rt50))+base_end

        rt10 = lpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(light_array[base_end:xlpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, light_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, light_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, light_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
    
        ###End baseline
        endline = np.mean(light_array[-2000:])
        endlineRMS = np.std(light_array[-2000:])
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, lmin, pedestal]
    
    except ValueError :
        result = [-1]*15
        
    return result

def light_ana_amore(light_array, conversion_factor, sampling_rate, x1, x2):
    try : 
        move_avg = 15

        diff_array = np.diff(movingaverage(light_array[x1:x2], move_avg))
        diff_array = diff_array[move_avg:-move_avg]
        diffmaxx = np.argmax(diff_array) + x1 + move_avg 
    
        grad_array = np.gradient(movingaverage(light_array[x1:x2], move_avg))
        grad_array = grad_array[move_avg:-move_avg]
        gradmaxx = np.argmax(grad_array) + x1 + move_avg

        diffmax = light_array[diffmaxx]
        gradmax = light_array[gradmaxx]

        #pulse height and height point

        base_end = gradmaxx - 400
        pedestal = np.mean(light_array[base_end-100:base_end])
        lmin = np.min(light_array[gradmaxx:gradmaxx+1000])
        peaks, _ = find_peaks(light_array[gradmaxx:gradmaxx+1000], height=pedestal+10)
        xlpeaks = peaks+x1
        lpeak = np.max(light_array[xlpeaks]) - pedestal
        arglpeak = np.argmax(light_array[xlpeaks])
        xlpeak = xlpeaks[arglpeak]

        #For Risetime
        rt90 = lpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(light_array[base_end:xlpeak] - rt90))+base_end

        rt50 = lpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(light_array[base_end:xlpeak] - rt50))+base_end

        rt10 = lpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(light_array[base_end:xlpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, light_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, light_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, light_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
    
        ###End baseline
        endline = np.mean(light_array[-2000:])
        endlineRMS = np.std(light_array[-2000:])
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, lmin, pedestal, diffmaxx, diffmax, gradmaxx, gradmax]
    
    except ValueError :
        result = [-1]*19
        
    return result

#def light_ana(light_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
#    try :
#        lpeak = np.max(light_array[base_end: base_end+1000]) - baseline
#        xlpeak = np.argmax(light_array[base_end : base_end+1000]) +base_end
#        result = [xlpeak, lpeak]
#    
#    except ValueError :
#        result = [-1. -1]
#      
#    return result

###HEAT Channel filter analysis###
def heat_filter_ana(lowcut, highcut, order, heat_array, conversion_factor, sampling_rate, base_end) :
    try :
        #print ("in func ----- nofileter : ", heat_array[0:10])
        y = butter_bandpass_filter_sos(heat_array, lowcut, highcut, sampling_rate, order=order)
        #print ("in func ----- fileter : ", y[0:10]) 
        fbaseline = np.mean(y[int(base_end)-1500:int(base_end)])
        fbaselineRMS = np.std(y[int(base_end)-1500:int(base_end)])

        peaks, _ = find_peaks(y[base_end:base_end+1000], height=fbaseline+10)
        xhpeaks = peaks+base_end
        hpeak = np.max(y[xhpeaks]) - fbaseline
        arghpeak = np.argmax(y[xhpeaks]) 
        xhpeak = xhpeaks[arghpeak]
        
        yy = 9999
        xx = 0
        
        for ii in range(xhpeak, xhpeak+1000, 1) :
            #if (y[ii] > yy) : break
            if (y[ii] < yy) :
                yy = y[ii]
                xx = ii
        
        lfmin = yy
        xlfmin = xx
        
        hpeak2nd = np.max(y[xlfmin:xlfmin+1000])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin+1000]) + xlfmin
        
        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd]
        
    except ValueError :
        result = [-1]*8
        
    return result

###LIGHT Channel filter analysis###
def light_filter_ana(lowcut, highcut, order, light_array, conversion_factor, sampling_rate, base_end) :
    try :
        y = butter_bandpass_filter_sos(light_array-light_array[0], lowcut, highcut, sampling_rate, order=order)
        
        fbaseline = np.mean(y[int(base_end)-1000:int(base_end)])
        fbaselineRMS = np.std(y[int(base_end)-1000:int(base_end)])

        peaks, _ = find_peaks(y[base_end:base_end+1000], height=fbaseline+10)
        xhpeaks = peaks+base_end
        hpeak = np.max(y[xhpeaks])
        hpeak1 = np.max(y[xhpeaks]) - fbaseline
        arghpeak = np.argmax(y[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        
        yy = 9999
        xx = 0
        
        for ii in range(xhpeak, xhpeak+1000, 1) :
            if (y[ii] < yy) :
                yy = y[ii]
                xx = ii
        
        lfmin = yy
        xlfmin = xx
        
        hpeak2nd = np.max(y[xlfmin:xlfmin+1000])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin+1000]) + xlfmin
        
        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd, hpeak1]

        
    except ValueError :
        result = [-1]*9
        
    return result

def ch_filter_risetime(light_array, conversion_factor, sampling_rate, lowcut, highcut, order, base_end):
    try : 
        #pulse height and height point
        y = butter_bandpass_filter_sos(light_array-light_array[0], lowcut, highcut, sampling_rate, order=order)
        
        fbaseline = np.mean(y[7000:7700])
        fbaselineRMS = np.std(y[7000:7700])

        pedestal = fbaseline
        peaks, _ = find_peaks(y[base_end:base_end+1000], height=fbaseline+10)
        xlpeaks = peaks+base_end
        lpeak = np.max(y[xlpeaks])
        arglpeak = np.argmax(y[xlpeaks])
        xlpeak = xlpeaks[arglpeak]
        #lpeak = np.max(y[base_end:base_end+1000]) - pedestal
        #xlpeak = np.argmax(y[base_end:base_end+1000]) + base_end

        #For Risetime
        rt90 = lpeak*0.9
        arg_rt90 = np.argmin(abs(y[base_end:xlpeak] - rt90))+base_end

        rt50 = lpeak*0.5
        arg_rt50 = np.argmin(abs(y[base_end:xlpeak] - rt50))+base_end

        rt10 = lpeak*0.1
        arg_rt10 = np.argmin(abs(y[base_end:xlpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, y[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, y[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, y[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
    
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10]
    
    except ValueError :
        result = [-1]*11
        
    return result

    
###For Template Fitting def###
def template_fitting(axx, amb_array, tmp_axx, tmp_array, window0, window1, xpeak10_b, xpeak10) :
    try :
        OF_window0_b = int(xpeak10_b)-window0
        OF_window1_b = int(xpeak10_b)+window1
        
        axx_tmp_b = tmp_axx[OF_window0_b:OF_window1_b]-OF_window0_b
        ayy_tmp_b = tmp_array[OF_window0_b:OF_window1_b]
    
        OF_window0 = int(xpeak10)-window0
        OF_window1 = int(xpeak10)+window1

        axx_tmp = axx[OF_window0:OF_window1]-OF_window0
        ayy_tmp = amb_array[OF_window0:OF_window1]


        S_b = np.sum(ayy_tmp*ayy_tmp_b)/np.sum(ayy_tmp_b**2)
    
        samples = window1
        ayy_chi = ayy_tmp[window0:]
        ayy_chi_b = ayy_tmp_b[window0:]*S_b
    
        chi_b = np.sum((ayy_chi-ayy_chi_b)**2/ayy_chi_b)/(samples-1)
        
        result = [S_b, chi_b]
        
    except ValueError : 
        result = [-1]*2
        
    return result

###GET template fitting parameter###
def heat_ana_temp(heat_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
   
        #pulse height and height point
        peaks, _ = find_peaks(heat_array[base_end:base_end+500], height=baseline+0.5)
        xhpeaks = peaks+base_end
        hpeak = np.max(heat_array[xhpeaks]) - baseline
        arghpeak = np.argmax(heat_array[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        result = [xhpeak, hpeak]
        
        #For Risetime
        rt90 = hpeak*0.9+baseline
        arg_rt90 = np.argmin(abs(heat_array[base_end:xhpeak] - rt90))+base_end

        rt50 = hpeak*0.5+baseline
        arg_rt50 = np.argmin(abs(heat_array[base_end:xhpeak] - rt50))+base_end

        rt10 = hpeak*0.1+baseline
        arg_rt10 = np.argmin(abs(heat_array[base_end:xhpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, heat_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, heat_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
        
        result = [xhpeak, hpeak, rt90_f, rt50_f, rt10_f]
         
        return result

def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')
