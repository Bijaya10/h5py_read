import h5py
import numpy as np

f = h5py.File('test1.h5', 'w')

g = f.create_group("group")
sub = g.create_group("sub")
print (f.keys())
print (g.keys())

sub.attrs['desc'] = "hello, world"
print (sub.attrs['desc'])

a = sub.create_dataset("a", data=np.arange(20))
b = sub['b'] = np.arange(10)
print (sub.keys())

for k in sub.keys():
    dataset = sub[k]
    print (dataset)
    print (dataset.dtype)
    print (dataset[()])

print ('----------write hdf5 file----------')
with h5py.File('test2.h5', 'w') as f :
    a = np.arange(0,100,1)
    dset = f.create_dataset('mydataset', (100,), data=a)
    print (dset.name)
    print (f.name)
    print (dset.shape)

    grp = f.create_group('subgroup')
    dset1 = grp.create_dataset('another_dataset', (50,), dtype='f')
    print (dset1.name)
     
    dset2 = f.create_dataset('subgroup2/dataset_three', (10,), dtype='i')
    print (dset2.name)

print ('----------read hdf5 file----------')

def printname(name):
    print(name)

with h5py.File('test2.h5', 'r') as f :
    for name in f :
        print (name)
    print ("Exist dataset? : ", "mydataset" in f) 
    print ("Exist dataset? : ", 'no?' in f)
    f.visit(printname)
    print (f['mydataset'][:])
