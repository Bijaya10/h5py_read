import numpy as np
import h5py
import pandas as pd

df = pd.DataFrame({'A' : [1,2,3], 'B' : [2,3,4]})
for i in range(10) :
    df2 = pd.DataFrame({'A' : [i], 'C' : [i*2]})
    df = df.append(df2, ignore_index=True)

df.to_hdf('test_pd_h5.h5', key='df', mode='w')

s = pd.DataFrame({'A' : [1,2,3], 'B' : [2,3,4]})
s.to_hdf('test_pd_h5.h5', key='s')

p1 = pd.read_hdf('test_pd_h5.h5', 'df')
p2 = pd.read_hdf('test_pd_h5.h5', 's')
a = p1['A']
b = p2['A']
print (p1)
print (p2)
