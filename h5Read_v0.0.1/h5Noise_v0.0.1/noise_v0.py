import ROOT, os, sys, glob, datetime
import numpy as np
import matplotlib.pyplot as plt
import h5py
from array import array
from scipy.signal import *


def butter_bandpass (lowcut, highcut, fs, order=5) :
    nyq = 0.5*fs
    low = lowcut/nyq
    high = highcut/nyq
    b,a = butter(order, [low, high], btype='band')
    return b, a
def butter_bandpass_filter(data, lowcut, highcut, fs, order=5) :
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

var1 = sys.argv[1]

runN = var1

path = '/data/AMoRE/users/kimwootae/RAW/2020_2nd_RODY/' + runN + '/' + 'amorecon*h5*'

filelist = sorted(glob.glob(path), key=os.path.getmtime)

PTRG_filelist = filelist[::10]

ch = array('f', [0]*50000)
fout = ROOT.TFile('/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.1/h5Noise_v0.0.1/{0}_noise1.root'.format(runN), 'recreate')
tr = ROOT.TTree('event', 'Noise waveform')
tr.Branch('ch', ch, 'ch[50000]/F')

for i in PTRG_filelist :
    print (datetime.datetime.now(), "--", i)
    try :
        with h5py.File(i, 'r') as f :
            dset = f['rawdata']
            nadc = []
            
            for j in range(int(dset.shape[0])):
                data = np.array(dset[j][2])
                ch3 = data[0][3]
                nadc.append(ch3)

                if (len(nadc) <= 2) : continue
                if (len(nadc) > 2) :
                    del nadc[0]

                aa = np.reshape(np.array(nadc), -1)
                aa = np.array(aa[:50000], dtype='int')

                b = butter_bandpass_filter(aa-aa[0], 300, 500, 100e+3, 1)
                peaks, _ = find_peaks(aa-np.min(aa), height=800, width=100)
                if (len(peaks) >= 1) : continue
                if (abs(np.mean(aa[:100]) - np.mean(aa[-100:])) >= 200) : continue
                if (np.max(b) >= 50 ) : continue

                bb = (aa-aa[0])*10./2**18

                for ii in range(50000) :
                    ch[ii] = bb[ii]
                
                tr.Fill()
                for ii in range(50000) :
                    ch[ii] = 0
                nadc = []
            
    except OSError : continue

tr.Write()
fout.Close()
