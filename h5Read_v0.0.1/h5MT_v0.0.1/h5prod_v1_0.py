import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys
import ROOT

from array import array
from scipy.signal import *

var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]
var4 = sys.argv[4]

h5_path = var1
subrunNum = var2
runN = var3
mergeN = int(var4)

'''Input Run Number'''
#h5files = h5_path+'*{0}*h5*'.format(runN)
h5files = h5_path+'*h5*'
OutPutPath = '/data/AMoRE/users/kimwootae/PROD/2020_2nd_RODY/{0}'.format(runN)

'''Sort files'''
def SORTINGFILE(n_files, mergeN) :
    #files = sorted(glob.glob(n_files), key=os.path.getmtime)
    files = sorted(glob.glob(n_files))
    n = len(files)
    infiles = []
    tfileGrup = []
    i = 0
    for filename in files :
        infiles.append(filename)
        if(len(infiles) == mergeN) :
            i += 1
            tfileGrup.append(infiles)
            infiles = []
        elif((len(files)//mergeN)==i and (len(files)%mergeN == len(infiles))) :
            tfileGrup.append(infiles)
    return tfileGrup

'''bandpass filter'''
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5*fs
    low = lowcut/nyq
    high = highcut/nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

'''TRIGGER'''
'''
def TRG(ch_array, thr) :
    ad = ch_array - thr

    ## 1st trigger
    n_trg1 = np.prod([ad[1:], ad[:-1]], axis=0)
    trg1 = (n_trg1 < 0)
    trg1 = np.append(trg1, False)

    ## 2nd trigger
    n_trg2 = np.diff(ch_array)
    n_trg2 = np.append(n_trg2, 0)
    trg2 = (n_trg2 > thr*0.01)

    ## Finial trigger
    trgf = np.all([trg1, trg2], axis=0)

    return np.where(trgf==True)
'''
'''temporary trigger logic'''
def TRG(ch_array, thr) :
    peaks, _ = find_peaks(ch_array, height=thr, prominence=1, width=50)
    return peaks

'''READ HDF5 FILE AND USING TRIGGER, OUTPUT:event array'''

def READ5PY (file_array) :
    init = 18000
    fini = 32000
    lowcut = 200
    highcut = 500
    fs = 100e+3

    maxn = 50000
    ncrystal = 4
    nch = ncrystal*2

    mt = [[] for _ in range(nch)]
    mtRMS = [[] for _ in range(nch)]
    
    for i in range(nch) :
        mt[i] = array('f', [0])
        mtRMS[i] = array('f', [0])

    fout = ROOT.TFile("{0}/MT_{1}_{2}.root".format(OutPutPath, runN, subrunNum), "recreate")
    #fout = TFile("{0}/data/{1:03d}_{2:05d}.root".format(OutPutPATH, int(RunNum), int(subrunNum)), "recreate")
    tr = ROOT.TTree("event", "1st Production event")

    for ii in range(nch) :
        tr.Branch("mt_{0}".format(ii), mt[ii], "mt_{0}[1]/F".format(ii))
        tr.Branch("mtRMS_{0}".format(ii), mtRMS[ii], "mtRMS_{0}[1]/F".format(ii))

    for i in file_array :
        print (i)
        with h5py.File(i, 'r') as f :
            print ("Keys : %s" %f.keys())
            print (list(f.keys()))

            dset = f['rawdata']
            print (dset.dtype)
            print (dset.shape)

            mod = 1
            lch = 8
            gch = mod*lch
            print ("total global channel : ", gch)

            for ii in range(int(dset.shape[0])) :
                data = np.array(dset[ii][2])
                tt = np.array(dset[ii][1])
                if ((ii == 0) or (ii % 100 == 0)) :
                    print (datetime.datetime.now())
                for k in range(lch) :
                    mt[k][0] = np.mean(data[0][k])
                    mtRMS[k][0] = np.std(data[0][k])
                    
                tr.Fill()
    
    tr.Write()
    fout.Close()


'''RUN FUNCTION'''
def run() :
    a = SORTINGFILE(h5files, mergeN)
    inputfiles = a[int(subrunNum)]
    READ5PY(inputfiles)

if __name__ == '__main__' :
    run()
