import h5py
import numpy as np
import matplotlib.pyplot as plt

def fft(ayy_tmp_ch3, sampling_rate, sampling_interval, idraw) :
    n_sample = len(ayy_tmp_ch3)
    t = np.arange(0,n_sample*sampling_interval, sampling_interval)
    k = np.arange(n_sample)
    T = n_sample/sampling_rate
    frq_bin = 1/T
    frq = k/T
    frq = frq[:n_sample//2]
    Yr = np.fft.fft(ayy_tmp_ch3)
    Y = Yr/n_sample
    Y = Y[:n_sample//2]
    if idraw ==1 :
        plt.loglog(frq, 2*abs(Y)/np.sqrt(frq_bin),'r')

    return frq, 2*abs(Y)/np.sqrt(frq_bin), Yr


sampling_rate = 100e+3
sampling_interval = 1./sampling_rate
idraw = 0

with h5py.File('AMOREADCCONT_000001.h5.00000', 'r') as f :
    print ("Keys : %s" % f.keys())
    print (list(f.keys()))

    dset = f['rawdata']
    print (dset.dtype)
    print (dset.shape)

    for i in range(24) :
        
        data = np.array(dset[i][2])
        for j in range(3) :
            for k in range(16) :
                print ("mod : ", j,", ch : ", k , ", : ", len(data[j][k]))
        '''
        plt.close()

        for j in range(3) :
            plt.ion()
            fig, ax = plt.subplots(4, 4,figsize=(3*4,3*4))
            for k in range(16) :
                print (k, " : ", len(data[j][k]))
                if (j == 2 and k > 3) :
                    break
                a, b, c = fft(data[j][k], sampling_rate, sampling_interval, idraw)
                ax[int(k//4)][int(k%4)].plot(np.array(a), np.array(b), linewidth=0.5, label='channel : {0}'.format(k))
                #ax[int(k//4)][int(k%4)].set_ylim(np.min(np.array(b)), b[2]*100)
                #ax[int(k//4)][int(k%4)].grid(True)
                ax[int(k//4)][int(k%4)].set_ylabel("V/$\sqrt{Hz}$", fontsize=5)
                ax[int(k//4)][int(k%4)].set_yscale('log')
                ax[int(k//4)][int(k%4)].set_xscale('log')
                #ax[int(k//4)][int(k%4)].legend()
            plt.subplots_adjust(wspace=0.1, hspace=0.1)
            plt.draw()
        '''
        a = input()

        if (a == 'q') :
            break
        else :
            plt.close()
        
        #plt.clf()
#       print (data.shape)
