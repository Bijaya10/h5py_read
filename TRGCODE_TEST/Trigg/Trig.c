#include <stdio.h>
#include <Python.h>
#include <numpy/arrayobject.h>
#include "trig.h"

static char module_docstring[] =
  "This module provides an interface for calculating chi-squared using C.";
static char trig_docstring[] =
  "Calculate the chi-squared of some data given a model.";

static PyObject *Trig(PyObject *self, PyObject *args);

static PyMethodDef module_methods[] = {
  {"Trig", Trig, METH_VARARGS, trig_docstring},
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef trigmodule = {
  PyModuleDef_HEAD_INIT,
  "Trig",   /* name of module */
  trig_docstring, /* module documentation, may be NULL */
  -1,       /* size of per-interpreter state of the module,
	       or -1 if the module keeps state in global variables. */
  module_methods
};

PyMODINIT_FUNC PyInit_Trig(void)
{
  PyObject *m = PyModule_Create(&trigmodule);
  if (m == NULL)
    return;

  /* Load `numpy` functionality. */
  import_array();
  return m;
}

static PyObject *Trig(PyObject *self, PyObject *args)
{
  PyObject * array1;

  if (!PyArg_ParseTuple(args, "O", &array1))
    return NULL;

  int ndp = 4096;
  PyObject *adc_array1 = PyArray_FROM_OTF(array1, NPY_INT, NPY_IN_ARRAY);

  int * c_adc1 = (int*)PyArray_DATA(adc_array1);
  
  int where = trig(c_adc1, ndp);
  //if(where != 0){ 
  //  printf("%d\n", where); 
  //} 
  Py_DECREF(adc_array1);

  PyObject *ret = Py_BuildValue("i" , where);

  return ret;
}

